package se.metsi.fileexplorer.services;

import com.vaadin.flow.component.AbstractField;
import com.vaadin.flow.component.HasValue;
import com.vaadin.flow.component.contextmenu.ContextMenu;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.grid.ItemClickEvent;
import com.vaadin.flow.component.html.H6;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.html.NativeButton;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.spring.annotation.SpringComponent;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.SystemUtils;
import org.springframework.beans.factory.annotation.Autowired;
import se.metsi.fileexplorer.data.FileSystemDataProvider;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

@SpringComponent
public class ContextMenuService {
    private ContextMenu contextMenu;
    private FileSystemDataProvider dataProvider;

    /**
     * Instantiates a ContextMenuService and sets up file explorer tree.
     *
     * @param dataProvider Injects {@link FileSystemDataProvider}
     */
    public ContextMenuService(@Autowired FileSystemDataProvider dataProvider) {
        this.dataProvider = dataProvider;
    }

    /**
     * Display context menu for the selected grid row
     * @param itemClickEvent
     */
    public void showContextMenu(ItemClickEvent<File> itemClickEvent) {
        if(contextMenu == null) {
            contextMenu = new ContextMenu();
        }

        // Resets the context menu, removing all previous items
        contextMenu.removeAll();
        // Set selected grid row as target of context menu actions
        contextMenu.setTarget(itemClickEvent.getSource());

        contextMenu.addItem("Create new directory", event -> {
            contextMenu.close();

            createNewDialog(itemClickEvent.getItem(), true);
        });

        contextMenu.addItem("Create new file", event -> {
            contextMenu.close();

            createNewDialog(itemClickEvent.getItem(), false);
        });

        contextMenu.addItem("Delete " + (itemClickEvent.getItem().isDirectory() ? "directory" : "file"), event -> {
            contextMenu.close();

            deleteDialog(itemClickEvent.getItem(), itemClickEvent.getItem().isDirectory());
        });

        // Only files can be edited
        if(itemClickEvent.getItem().isFile()) {
            contextMenu.addItem("Edit file", event -> {
                contextMenu.close();

                editFile(itemClickEvent.getItem());
            });
        }
    }

    /**
     * Create and open dialog with a text field for the user to provide the name of the new file or directory.
     * @param file Base path of where the new file or directory should be created
     * @param isDirectory Should a directory be created?
     */
    private void createNewDialog(File file, boolean isDirectory) {
        Dialog dialog = new Dialog();
        dialog.add(new H6("Press esc to close"));
        dialog.add(new Label("Enter name of " + (isDirectory ? "directory ": "file ")));

        // Set up action to be taken when user pressed 'Enter' key
        TextField nameTextField = new TextField((HasValue.ValueChangeListener<AbstractField.ComponentValueChangeEvent<TextField, String>>) textFieldEvent -> {
            if(isDirectory) {
                createNewDirectory(file, textFieldEvent.getValue());
            }
            else {
                createNewFile(file, textFieldEvent.getValue());
            }

            // Refresh data provider so that the changes show in file explorer.
            dataProvider.refreshAll();
            dialog.close();
        });
        nameTextField.focus();

        dialog.add(nameTextField);

        dialog.open();
    }

    /**
     * Create new file at selected location.
     * @param file The base path where the file is to be created.
     * @param value Filename including file extension (ex. 'test.txt'). Directories can be a part the filename.
     */
    private void createNewFile(File file, String value) {
        File newFile = new File(file.getAbsolutePath() + File.separator + value);

        // Create directories if not present. Could happen if value includes directories
        newFile.getParentFile().mkdirs();
        try {
            FileWriter writer = new FileWriter(newFile);
            writer.close();
        } catch (IOException e) {
            System.err.println("Could not create file");
        }
    }

    /**
     * Create new directory a specified location.
     * @param directory The base path where the directory is to be created.
     * @param value Name of the directory.
     */
    private void createNewDirectory(File directory, String value) {
        File newDirectory = new File(directory.getAbsolutePath() + File.separator + value);

        System.out.println(newDirectory.mkdirs() ? "Created " : "Could not create " + "directory: " + directory.getAbsolutePath() + File.separator + value);
    }

    /**
     * Opens up a dialog for the user to confirm deletion of file or directory.
     * @param file File with path to be deleted.
     */
    private void deleteDialog(File file, boolean isDirectory) {
        Dialog dialog = new Dialog();
        dialog.setCloseOnEsc(false);
        dialog.setCloseOnOutsideClick(false);

        dialog.add(new H6("Delete " + (isDirectory ? "directory?" : "file?")));

        // Confirm button with action to delete file or directory
        NativeButton confirmButton = new NativeButton("Yes", event -> {
            if(isDirectory) {
                try {
                    // Delete directory recursively
                    FileUtils.deleteDirectory(file);
                    System.out.println("Deleted directory (" + file.toPath() + ") and all its children");
                } catch (IOException e) {
                    System.err.println("Could not delete directory " + file.toPath());
                }
            }
            else {
                System.out.println(file.delete() ? "Deleted " : "Count not delete " + file.toPath());
            }

            // Refresh data provider so that the changes show in file explorer.
            dataProvider.refreshAll();
            dialog.close();
        });

        // Close dialog, cancelling deletion of file
        NativeButton cancelButton = new NativeButton("Cancel", event -> {
            dialog.close();
        });

        // Add buttons to dialog
        dialog.add(confirmButton, cancelButton);

        dialog.open();
    }

    /**
     * Open selected file using default application.<br />
     * Supports Windows, Linux and MacOS.
     * @param file File to be edited.
     */
    private void editFile(final File file) {
        if(file == null) {
            System.err.println("Selected file cannot be null");
        }

        try {
            String command;

            if(SystemUtils.IS_OS_WINDOWS) {
                command = "RUNDLL32.EXE SHELL32.DLL,OpenAs_RunDLL ";
            }
            else if(SystemUtils.IS_OS_LINUX) {
                command = "edit ";
            }
            else if(SystemUtils.IS_OS_MAC) {
                command = "open ";
            }
            else {
                System.out.println("Not a recognised operating system");

                return;
            }

            // Tries to open the file using OS specified command. Ex 'edit \home\test\test.txt'
            Runtime.getRuntime().exec(command + file.toPath());

            // Refresh data provider so that the changes show in file explorer.
            dataProvider.refreshAll();
        } catch (IOException e) {
            System.err.println("Could not edit file " + file.toPath());
        }
    }
}
