package se.metsi.fileexplorer.views;

import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.ItemClickEvent;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.treegrid.TreeGrid;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.PWA;
import org.springframework.beans.factory.annotation.Autowired;
import se.metsi.fileexplorer.data.FileSystemDataProvider;
import se.metsi.fileexplorer.services.ContextMenuService;

import java.io.File;

@Route
@PWA(name = "File Explorer", shortName = "File Explorer")
public class MainView extends VerticalLayout {
    private ContextMenuService contextMenuService;
    private FileSystemDataProvider dataProvider;

    /**
     * Instantiates a new MainView and sets up file explorer tree.
     *
     * @param contextMenu Injects {@link ContextMenuService}
     * @param dataProvider Injects {@link FileSystemDataProvider}
     */
    public MainView(@Autowired ContextMenuService contextMenu, @Autowired FileSystemDataProvider dataProvider) {
        this.contextMenuService = contextMenu;
        this.dataProvider = dataProvider;

        setupFileExplorer();
    }

    /**
     * Sets up main page and adds relevant components
     */
    private void setupFileExplorer() {
        add(new H1("File Explorer"));

        File[] systemDrives = File.listRoots();

        // Drop-down list of available system drives
        Select<File> drives = new Select<>();
        drives.setItems(systemDrives);
        drives.setValue(systemDrives[0]);
        drives.addValueChangeListener(event -> {
            // Change selected drive
            dataProvider.setRootAndRefresh(event.getValue());
        });
        add(drives);

        addFileItemsTree();
    }

    /**
     * Tree grid that displays the files and directories on target drive
     */
    private void addFileItemsTree() {
        TreeGrid<File> fileItemsTreeGrid = new TreeGrid<>();
        fileItemsTreeGrid.setDataProvider(dataProvider);
        fileItemsTreeGrid.setSelectionMode(Grid.SelectionMode.SINGLE);

        // Add context menu to selected item
        fileItemsTreeGrid.addItemClickListener((ComponentEventListener<ItemClickEvent<File>>) componentEvent ->
                contextMenuService.showContextMenu(componentEvent));

        // Add columns to tree
        fileItemsTreeGrid.addHierarchyColumn(File::getName).setHeader("Filename");
        fileItemsTreeGrid.addColumn(File::getAbsolutePath)
                .setHeader("Path");

        add(fileItemsTreeGrid);
    }
}
