package se.metsi.fileexplorer.data;

import com.vaadin.flow.data.provider.hierarchy.AbstractBackEndHierarchicalDataProvider;
import com.vaadin.flow.data.provider.hierarchy.HierarchicalQuery;
import com.vaadin.flow.spring.annotation.SpringComponent;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.stream.Stream;

/**
 * Fetches data to be displayed in grid
 */
@SpringComponent
public class FileSystemDataProvider extends AbstractBackEndHierarchicalDataProvider<File, Void> {
    private File root;

    /**
     * Instantiates a new FileSystemDataProvider and sets first root directory from OS.
     */
    public FileSystemDataProvider() {
        this.root = File.listRoots()[0];
    }

    /**
     * Get a count of children
     * @param query
     * @return Amount of children in queried path
     */
    @Override
    public int getChildCount(HierarchicalQuery<File, Void> query) {
        return (int) fetchChildren(query).count();
    }

    /**
     * Get children from queried path or root directory.
     * @param query
     * @return Stream of new files and directories.
     */
    @Override
    public Stream<File> fetchChildrenFromBackEnd(HierarchicalQuery<File, Void> query) {
        File parent = query.getParentOptional().orElse(root);

        return Arrays.stream(parent.listFiles())
                .filter(file -> !isHiddenOrSymbolicFilePath(file));
    }

    /**
     * Checks if path has any children.
     * @param file Path to be checked.
     * @return 'true' if children are present, otherwise 'false'.
     */
    @Override
    public boolean hasChildren(File file) {
        return file.list() != null
                && file.list().length > 0;
    }

    /**
     * Set new root and refreshes the data provider.
     * @param root Path to new root directory
     */
    public void setRootAndRefresh(File root) {
        this.root = root;
        refreshAll();
    }

    /**
     *
     * @param file file to be checked.
     * @return 'true' if file is hidden or a symbolic link, otherwise 'false'.
     */
    private boolean isHiddenOrSymbolicFilePath(File file) {
        try {
            return Files.isHidden(file.toPath()) || Files.isSymbolicLink(file.toPath());
        }
        catch (IOException e) {
            System.err.println("Could not read file at path " + file.toPath());
            return false;
        }
    }
}
