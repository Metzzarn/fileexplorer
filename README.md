# File Explorer

## Running the program
.jar available on [download](https://bitbucket.org/Metzzarn/fileexplorer/downloads/) page.

Run using java -jar <file>

Go to localhost:8090 in a web browser.

## About
A web based file explorer using the open source framework [Vaadin](https://vaadin.com/).

The first available drive on the system is used by default.

Ability to browse the file system, create/delete/edit new directories and files.

Tested on Windows.